import { combineReducers } from "redux";
import newsReducer from "./newsReducer";
import pagesReducer from "./pagesReducer";
import userReducer from './userReducer';

export default combineReducers({
  news: newsReducer,
  pages: pagesReducer,
  user: userReducer
});

