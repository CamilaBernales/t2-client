import {
  AUTHENTICATE_USER,
  EDIT_USER,
  LOGOUT_USER,
} from "../types";

const initialState = {
  user: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case AUTHENTICATE_USER:
      return {
        ...state,
        user: action.payload,
      };
    case EDIT_USER:
      return {
        ...state,
        user: action.payload,
      };
    case LOGOUT_USER:
      return {
        ...state,
        user: {},
      };
    default:
      return state;
  }
}
