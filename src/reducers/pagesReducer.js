import {
  GET_PAGE,
  GET_PAGE_DATA,
  GET_PAGE_TO_EDIT,
} from "../types";

const initialState = {
  pages: [],
  pageData: {},
  page: null,
  error: null,
  pageEdit: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_PAGE:
      return {
        ...state,
        page: action.payload,
      };
    case GET_PAGE_DATA:
      return {
        ...state,
        pageData: action.payload,
      };
    case GET_PAGE_TO_EDIT:
      return {
        ...state,
        pageToEdit: action.payload,
      };
    default:
      return state;
  }
}
