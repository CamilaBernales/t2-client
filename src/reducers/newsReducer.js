import {
  GET_NEWS,
  GET_NEW_REMOVE,
  NEW_DELETED_SUCCESS,
  NEW_DELETED_ERROR,
  GET_NEW
} from "../types";

const initialState = {
  news: [],
  error: null,
  newDeleted: null,
  new: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_NEWS:
      return {
        ...state,
        news: action.payload,
      };
    case GET_NEW_REMOVE:
      return {
        ...state,
        newDeleted: action.payload,
      };
    case NEW_DELETED_SUCCESS:
      return {
        ...state,
        news: state.news.filter((post) => post.id !== state.newDeleted),
        newDeleted: null,
      };
    case GET_NEW:
      return {
        ...state,
        new: action.payload,
      };
    default:
      return state;
  }
}
