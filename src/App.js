import React, { useEffect } from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import Routes from "./components/routes";
import Footer from "./components/footer/Footer";
import Header from "./components/header/Header";
import { useDispatch } from "react-redux";
import {
  authenticateUser
} from './actions/userAction'
import { getData } from "./components/api/users";

function App() {
  const dispatch = useDispatch()
  const token = localStorage.getItem('token')

  useEffect(async ()=>{
    if(token){
      const user = await getData()
      dispatch(authenticateUser(user.data))
    }
  }, [])

  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <div className="">
          <Routes />
        </div>
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
