import React from 'react'
import { Link } from 'react-router-dom'

const BtnSend =()=>{
    return(
       <div className="inline-clearfix d-flex justify-content-between">
         <button className='btn-primary' type="submit" value='send Messager'>Registrar cuenta</button>
         <p>
          ¿Ya posee una cuenta? <Link to='/login'>Inicie sesión</Link>
         </p>
       </div>
    )
}

export default BtnSend