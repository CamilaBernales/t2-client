import React, { Fragment } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import AlertService from "../alertService/Alert";
import BtnSend from "./BtnSend";
import { authenticateUser } from "../../actions/userAction";
import { useHistory } from "react-router";

const user = require("../api/users");

const Form = () => {
  const { register, handleSubmit, setError, errors } = useForm();
  const dispatch = useDispatch();
  const history = useHistory()

  const onSubmit = async (data, e) => {
    e.preventDefault();
    try {
      const response = await user.register(data);
      if (response.status === 200) {
        e.target.reset();
        AlertService({
          success: true,
          successMessage: "Usuario registrado!",
        }).then(() => {
          dispatch(authenticateUser(response.data));
          localStorage.setItem('user', JSON.stringify(response.data))
          history.push('/backoffice/profile')
        });
      } else {
        const { errors } = response.data;
        errors.map((element, index) => {
          return setError(`${element.param}`, {
            type: "manual",
            message: `${element.msg}`,
          });
        });
      }
    } catch (error) {
      AlertService({
        error: true,
        errorMessage:
          "No es posible conectarse al servidor, intente nuevamente en unos minutos",
      });
    }
  };

  return (
    <Fragment>
      <div className="form-login form-register mt-5">
        <h2 className='text-center'>Registrate</h2>
        <form
          id="registerForm"
          className="clearfix"
          onSubmit={handleSubmit(onSubmit)}
        >
          <div className="field">
            <input
              type="text"
              name="firstName"
              placeholder="Nombre"
              ref={register({
                required: { value: true, message: "Complete este campo" },
                maxLength: {
                  value: 30,
                  message: "El nombre debe tener maximo 30 caracteres",
                },
                minLength: {
                  value: 2,
                  message: "El nombre debe tener minimo 2 caracteres",
                },
              })}
            />
            <span className="text-danger text-small">
              {errors?.firstName?.message}
            </span>
          </div>
          <div className="field">
            <input
              type="text"
              name="lastName"
              placeholder="Apellido"
              ref={register({
                required: { value: true, message: "Complete este campo" },
                maxLength: {
                  value: 30,
                  message: "El nombre debe tener maximo 30 caracteres",
                },
                minLength: {
                  value: 2,
                  message: "El nombre debe tener minimo 2 caracteres",
                },
              })}
            />
            <span className="text-danger text-small">
              {errors?.lastName?.message}
            </span>
          </div>
          <div className="field">
            <input
              type="email"
              name="email"
              placeholder="E-mail"
              ref={register({
                required: { value: true, message: "Complete este campo" },
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: "Direccion de email inválida",
                },
              })}
            />
            <span className="text-danger text-small">
              {errors?.email?.message}
            </span>
          </div>
          <div className="field">
            <input
              type="password"
              name="password"
              placeholder="Contraseña"
              ref={register({
                required: { value: true, message: "Complete este campo" },
                maxLength: {
                  value: 20,
                  message: "La contraseña debe tener maximo 20 caracteres",
                },
                minLength: {
                  value: 6,
                  message: "La contraseña debe tener minimo 6 caracteres",
                },
              })}
            />
            <span className="text-danger text-small">
              {errors?.password?.message}
            </span>
          </div>
          <BtnSend />
        </form>
      </div>
    </Fragment>
  );
};

export default Form;
