import React, {Fragment} from 'react'
import Slider from '../api/slider'
import {useForm} from 'react-hook-form'
import { useHistory } from 'react-router';
import AlertService from "../alertService/Alert";


const FormSlide = () => {

    const { register, handleSubmit, errors } = useForm();
    const history = useHistory()


    function setSrc() {
        document.getElementById("imageContent").onchange = function (e) {
          let reader = new FileReader();
      
          reader.readAsDataURL(e.target.files[0]);
      
          reader.onload = function () {
            let preview = document.getElementById("preview"),
              image = document.createElement("img");
      
            image.src = reader.result;
      
            preview.innerHTML = "";
            preview.append(image);
          };
        };
      }

    
    const onSubmit = async (data) => {

        try {
            const formData = new FormData();
            formData.append("text", data.title)
            formData.append("order", data.order)
            formData.append("image", data.image[0]);
            
            const response = await Slider.create(formData)
            console.log(response);
            if (response.status === 200) {
                AlertService({
                  success: true,
                  successMessage: "Imagen cargada con exito!",
                }).then(() => {
                  history.push("/backoffice/slider");
                });
              } else {
                AlertService({
                  error: true,
                  errorMessage:
                    "No es posible cargar la imagen, intente nuevamente en unos minutos",
                });
              }
            
        } catch (error) {
            console.log(error);
            AlertService({
              error: true,
              errorMessage:
                "No es posible cargar la imagen, intente nuevamente en unos minutos",
            });
        }
        
    }

    return(
          <Fragment>
            <div className='form-login form-register'>
                    <h2 className='mt-4'>Agregar Slides</h2>
                    <form id='registerForm' className='clearfix' onSubmit={handleSubmit(onSubmit)}>
                        <div className="form-group row">
                            <label htmlFor="title" className="col-sm-2 col-form-label">
                                    T&iacute;tulo
                            </label>
                                <div className="col-sm-10">
                                    <input
                                        name="title"
                                        type="text"
                                        className="form-control"
                                        ref={register({
                                        required: { value: true, message: "Complete este campo" },
                                        })}
                                    />
                                    <span className="text-danger text-small">
                                        {errors?.title?.message}
                                    </span>
                                </div>
                        </div>
                        
                    <div className="form-group row">
                            <label className="col-sm-2 col-form-label">
                               Orden
                            </label>
                            <div className="col-sm-10">
                                <select
                                    name="order"
                                    className="custom-select"
                                    ref={register({
                                    required: {
                                        value: true,
                                        message: "Seleccione el orden del Slide",
                                    },
                                    })}
                                >
                                    <option></option>
                                    <option value="1"> 1</option>
                                    <option value="2"> 2</option>
                                    <option value="3"> 3</option>
                                </select>
                            <span className="text-danger text-small">
                                {errors?.order?.message}
                            </span>
                            </div>
                    </div>


                    <div className="form-group row">
                            <label className='col-sm-2 col-form-label'>Imagen</label>
                                <div className="col-sm-10">
                                <input
                                        name="image"
                                        type="file"
                                        className="form-control-file"
                                        id="imageContent"
                                        onClick={setSrc}
                                        accept="image/x-png,image/gif,image/jpeg"
                                        ref={register({
                                            required: {
                                            value: true,
                                            message: "Seleccione una imagen",
                                            },
                                        })}
                                        />
                                    <span className="text-danger block text-small text-center">
                                    {errors.image && errors?.image.message}
                                    </span>
                                    <div id="preview"></div>
                              </div>
                        </div>

                      <div className="inline-clearfix">
                         <button className='btn-primary mb-4' type="submit">Agregar</button>
                      </div>
                    </form>    
            </div>
        </Fragment>
    )
}

export default FormSlide
