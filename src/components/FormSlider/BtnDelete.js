import React, { Fragment } from "react";
import Slider from '../api/slider'
import AlertService from '../alertService/Alert'

const BtnDelete = ({id}) => {

  const confirmeAction = async () => {
    try {
        const response = await Slider.deleteSlide(id)
        console.log(response);
            if (response.status === 200) {
                AlertService({
                  success: true,
                  successMessage: "Slide eliminado con exito!",
                }).then(() => {
                  window.location.pathname = "/backoffice/slider";
                });
              } else {
                AlertService({
                  error: true,
                  errorMessage:
                    "No es posible eliminar el slide, intente nuevamente en unos minutos",
                });
              }
            
        } catch (error) {
            console.log(error);
            AlertService({
              error: true,
              errorMessage:
                "No es posible cargar la imagen, intente nuevamente en unos minutos",
            });
        }
        
    }

  return (
    <Fragment>
      <input
        className="btn btn-danger ml-2"
        type="button"
        onClick={confirmeAction}
        value="ELIMINAR"
      />
    </Fragment>
  );
};

export default BtnDelete;
