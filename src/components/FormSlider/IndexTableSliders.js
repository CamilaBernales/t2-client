import React, {useState, useEffect, Fragment} from 'react'
import Slider from '../api/slider'
import BtnDelete from './BtnDelete'
import {Link} from 'react-router-dom'

const IndexTableSliders = () => {

    const [sliders, setSliders] = useState([]);

    useEffect(() => {
      getSliders();
    }, []);

    const getSliders = async () => {
      try {
        const res = await Slider.getSliders();
        setSliders(res.data);
      } catch (error) {
        console.log(error);
      }
    };
  

    return (
        <Fragment>
          <div className="account-content backed-campaigns account-table">
            <h3 className="account-title">Slides</h3>
            <div className="account-main table-container">
              <table className="table">
                <thead className="thead-dark">
                  <tr>
                    <th>TEXTO</th>
                    <th className="text-center">IMAGEN</th>
                    <th className="text-center">ACCION</th>
                  </tr>
                </thead>
                <tbody>
                  {sliders.map((n) => {
                    return (
                      <tr key={n.id}>
                        <td>{n.text}</td>
                        <td className="text-center"><img className="logoZg" src={n.image} alt="slide" /></td>
                        <td className="text-center">
                          <BtnDelete id={n.id} />
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
          <Link className="btn-primary mb-4 ml-3 float-xl-left" to="slider/create">
            Nueva
          </Link>
        </Fragment>
      );
}

export default IndexTableSliders