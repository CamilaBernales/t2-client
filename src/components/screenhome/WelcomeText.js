import React, { useEffect, useState } from "react";
import organization from "../api/organization";

const WelcomeText = () => {

  const [ welcomeText, setWelcomeText ] = useState()
  const [ ongTitle, setOngTitle ] = useState()

  useEffect(()=>{
    const getData = async ()=>{
      try {
        const response = await organization.getData()
        setWelcomeText(response.data.welcomeText)
        setOngTitle(response.data.name)
      } catch (error) {
        
      }      
    }

    getData()
  }, [])

  return (
    <div className="container mt-2 mb-5">
      <h1 className="my-3 text-center">
        {ongTitle}
      </h1>
      <p className='text-left'>{welcomeText}</p>
    </div>
  );
};

export default WelcomeText;
