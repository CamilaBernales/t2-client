import React, { useState, useEffect } from "react";
const slider = require("../api/slider");

const Slider = () => {
  const [sliders, setSliders] = useState([]);
  useEffect(() => {
    getSliders();
  }, []);
  const getSliders = async () => {
    try {
      const res = await slider.getSliders();
      setSliders(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div
      id="carouselExampleControls"
      className="carousel slide"
      data-ride="carousel"
    >
      <div className="carousel-inner">
        {sliders.map((element, index) => (
          <div key={index}
            className={index === 0 ? "carousel-item active" : "carousel-item"}
          >
            <img className="d-block w-100 slidersImg" src={element.image} alt="slide" />
          </div>
        ))}
      </div>
      <a
        className="carousel-control-prev"
        href="#carouselExampleControls"
        role="button"
        data-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="sr-only ">Previous</span>
      </a>
      <a
        className="carousel-control-next"
        href="#carouselExampleControls"
        role="button"
        data-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="sr-only">Next</span>
      </a>
    </div>
  );
};

export default Slider;
