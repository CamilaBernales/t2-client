import React,{useEffect} from "react";
import { Link, NavLink } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getNews } from "../api/new";
import './home.css'

const HomeNews = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    const fetchData = () => dispatch(getNews());
    fetchData();
    // eslint-disable-next-line
  }, []);

  const news = useSelector((state) => state.news.news);
  let reverseNews = []
  for (let index = news.length -1 ; index >=0 ; index--) {
    reverseNews.push(news[index])    
  }

  return (
    <div className="container m-auto activity-cards">
      <div className='d-flex justify-content-between'>
        <h2 className="my-3 py-3 text-left">Novedades</h2>
        <NavLink to='/news' className='align-self-center news-link'>Ver todas!</NavLink>
      </div>
      <div className="row m-auto ">
        {reverseNews.map((post, index) => {
          if(index < 3 ){
            return(
              <div key={post.id} className="col col-12 col-sm-12 col-md-6 col-lg-4 ">
                <div className="news-card my-3 py-4">
                  <img
                    className='card-img'
                    src={post.image}
                    alt="Formato Imagen Incorrecto"
                  />
                  <div className="card-body">
                    <h3 className="card-title">{post.title}</h3>
                    <Link to={`/news/detail/${post.id}`} className="btn btn-primary mt-3">
                      Ver más
                    </Link>
                  </div>
                </div>
              </div>
            )
          }
        })}
      </div>
    </div>
  );
};

export default HomeNews;
