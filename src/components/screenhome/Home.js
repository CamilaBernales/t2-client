import React from "react";
import Slider from "./Slider";
import WelcomeText from "./WelcomeText";
import Activity from "./Activity";
import {activities} from "./activities";
import HomeNews from "./HomeNews";
import './home.css';

const Home = () => {
  return (
    <div>
      <div className="m-auto">
        <Slider />
      </div>
      <WelcomeText />
      <Activity activities={activities} />
      <HomeNews/>
    </div>
  );
};

export default Home;
