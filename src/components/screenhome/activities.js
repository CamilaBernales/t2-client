const activities = [
  {
    title: "Cuarentena Solidaria",
    body:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
    iconClass: "fas fa-hand-holding-heart fa-2x py-2",
  },
  {
    title: "Formación de Líderes",
    body:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
    iconClass: "fas fa-book-reader fa-2x py-2",
  },
  {
    title: "Teach me something I can't Google",
    body:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
    iconClass: "fas fa-hand-holding-heart fa-2x py-2",
  },
];

export { activities };
