import React from 'react';
import ModalProfile from './ModalProfile';
 
class Profile extends React.Component {
	state={
		firstName:'Cristiano',
        lastName:'Ronaldo',
        email:'cr7@gmail.com',
        isToggleOn: true,
        isToggleSave: false,
        isToggleDisabled: true
    }
	handleEditClick = () => {
        this.setState(state => ({
            isToggleOn: !state.isToggleOn,
            isToggleSave: false,
            isToggleDisabled: !state.isToggleDisabled
          }));
	}
	onChange = (e) =>{
        this.setState({
            [e.target.name]:e.target.value,
            [e.target.name]:e.target.value,
            [e.target.name]:e.target.value
        })
    }
	onSubmit = (e)  =>{
        e.preventDefault();
		const user ={
            firstName:this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email
        }
        this.setState(state => ({
            isToggleOn: !state.isToggleOn,
            isToggleSave: !state.isToggleSave,
            isToggleDisabled: !state.isToggleDisabled
          }));
    }
    render(){
        const {isToggleOn,isToggleSave,isToggleDisabled,firstName,lastName,email} = this.state;
        const {handleEditClick,onSubmit,onChange} = this;
        return(
            <div className="container">
                <div className="d-flex">
                    <h1 className="mr-auto p-2 text-primary">Profile Settings</h1>
					<button className="d-flex justify-content-end bg-danger text-white border rounded" data-toggle="modal" data-target="#deleteModal">Delete account</button>
                </div>
                <ModalProfile />
                <hr />
                <form>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label className="d-flex justify-content-start">FirstName</label>
                            <input type="text" className="form-control"
                            name="firstName"
							value={firstName}
                            onChange={onChange} 
                            disabled={isToggleDisabled}
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <label className="d-flex justify-content-start">LastName</label>
                            <input type="text" className="form-control"
                            name="lastName"
							value={lastName}
							onChange={onChange} 
							disabled={isToggleDisabled}
                            />
                        </div>
                        <div className="form-group col-md-6">
                             <label className="d-flex justify-content-start">Email</label>
                             <input type="email" className="form-control"
                            name="email"
							value={email}
							onChange={onChange} 
							disabled={isToggleDisabled}
                             />
                        </div>
                    </div>
						{!isToggleOn &&
                        	<button className="btn btn-primary d-flex justify-content-start rounded" onClick={onSubmit}>Save</button>
                    	}
						{isToggleSave && <p className="alert alert-success">Profile Saved</p>}
                    </form>
					{isToggleOn &&
                        <button className="btn btn-primary d-flex justify-content-start rounded" onClick={handleEditClick}>Edit</button>   
                    }
            </div>
        )
    }
}
 
export default Profile;
