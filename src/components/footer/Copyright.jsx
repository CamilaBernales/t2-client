import React from 'react';

class Copyright extends React.Component {

    render(){
        return(
            <div className="footer-copyright">
                <div className="container">
                    <p className="copyright">2020 Alkemy. Todos los derechos reservados.</p>
                    <a href="/#" className="back-top">
                       Ir arriba
                        <span className="ion-android-arrow-up"/>
                    </a>
                </div>
            </div>
        )
    }
}

export default Copyright;