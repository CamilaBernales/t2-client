import React from 'react';
import organization from '../api/organization';

class SocialNetworks extends React.Component {
    constructor(props){
        super (props)
        this.state = {
            facebookLink: '',
            instagramLink: '',
            linkedinLink: ''
        }
    }
    
    async componentDidMount(){
        try {
            const response = await organization.getData()
            const { facebookUrl, instagramUrl, linkedinUrl } = response.data
            this.setState({
                facebookLink: facebookUrl,
                instagramLink: instagramUrl,
                linkedinLink: linkedinUrl
            })
        } catch (error) {
            this.setState({
                facebookLink: 'https://www.facebook.com/fundacionzonasgrises/?tn-str=k*F',
                instagramLink: 'https://www.instagram.com/fundacionzonasgrises/',
                linkedinLink: 'https://www.linkedin.com/company/fundaci%C3%B3n-zonas-grises/'
            })
        }
    }

    render(){
        const { facebookLink } = this.state;
        const { instagramLink } = this.state;
        const { linkedinLink } = this.state;
        return (
            <div className="footer-menu-item newsletter mt-4">
                <div className="follow">
                    <h3 className="text-white">Seguinos!</h3>
                    <ul>
                        <li className='facebook'>
                            <a href={facebookLink}>
                                <i className='fab fa-facebook' aria-hidden='true'/>
                            </a>
                        </li>
                        <li className='instagram'>
                            <a href={instagramLink}>
                                <i className='fab fa-instagram' aria-hidden='true'/>
                            </a>
                        </li>
                        <li className='facebook'>
                            <a href={linkedinLink}>
                                <i className='fab fa-linkedin' aria-hidden='true'/>
                            </a>
                        </li>               
                    </ul>
                </div>
            </div>
        )
    }
}

export default SocialNetworks;