import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { logged } from "../header/headerRoutes";
import { notLogged } from "../header/headerRoutes";
import FooterLink from "./Link";

const Organization = require("../api/organization");
const loggedRoutes = logged;
const notLoggedRoutes = notLogged;

const Logo = () => {
  const [companyName, setCompanyName] = useState();
  const [img, setImg] = useState();
  const [subMenu, setSubMenu] = useState(false);
  const [isLogged, setIsLogged] = useState(false);
  const { user } = useSelector((state) => state.user);

  useEffect(() => {
    const getData = async () => {
      try {
        const ong = await Organization.getData();
        if (ong.status === 200) {
          setCompanyName(ong.data.name);
          setImg(ong.data.image);
        } else {
          throw new Error();
        }
      } catch (error) {
        setCompanyName("Zonas Grises");
        setImg("images/assets/logo.png");
      }
    };

    if (user) {
      setIsLogged(true);
    } else {
      setIsLogged(false);
    }
    getData();
  }, [user]);

  const handleClick = (e) => {
    e.preventDefault();
    setSubMenu(!subMenu);
  };

  return (
    <div className="footer-menu-item">
      <div className="row">
        <div className="col mx-4 px-5">
          <div className="site-brand">
            <img className="logoZg" src={img} alt="logo" />
            <h3 className="mt-2 text-white">{companyName}</h3>
          </div>
        </div>
        <div className="col mx-4 px-5">
          <ul className="ml-2 mr-2">
            <h3 className="text-left text-white">Accesos</h3>
            {isLogged &&
              loggedRoutes.map((element, index) => {
                if (element.text === "Actividades") {
                  return (
                    <div key={index}>
                      <li className="text-left">
                        <NavLink
                          onClick={(e) => handleClick(e)}
                          to={element.route}
                        >
                          {element.text}
                        </NavLink>
                      </li>
                      <ul className={subMenu ? "d-block ml-3 mb-3" : "d-none"}>
                        {element.activities.map((activity, index) => {
                          return <FooterLink element={activity} key={index} />;
                        })}
                      </ul>
                    </div>
                  );
                } else {
                  return <FooterLink element={element} key={index} />;
                }
              })}
            {!isLogged &&
              notLoggedRoutes.map((element, index) => {
                if (element.text === "Actividades") {
                  return (
                    <div key={index}>
                      <li className="text-left">
                        <NavLink
                          onClick={(e) => handleClick(e)}
                          to={element.route}
                        >
                          {element.text}
                        </NavLink>
                      </li>
                      <ul className={subMenu ? "d-block ml-3 mb-3" : "d-none"}>
                        {element.activities.map((activity, index) => {
                          return <FooterLink element={activity} key={index} />;
                        })}
                      </ul>
                    </div>
                  );
                } else {
                  return <FooterLink element={element} key={index} />;
                }
              })}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Logo;
