import React from 'react';
import Copyright from './Copyright';
import Logo from './Logo';
import SocialNetworks from './SocialNetworks';
import './footer.css';

class Footer extends React.Component {

    render() {
        return (
            <footer className='site-footer mt-5 footer'>
                <div className="footer-menu">
                    <div className="container row d-flex justify-content-around">
                        <Logo />
                        <SocialNetworks />
                    </div>
                </div>
                <Copyright />
            </footer>
        )
    }
}

export default Footer;