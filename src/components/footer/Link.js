import React from 'react';
import { NavLink } from 'react-router-dom';

class FooterLink extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            element: props.element
        }
    }

    render(){
        const { element } = this.state;
        return(
            <li className='text-left'>
                <NavLink to={element.route}>
                    {element.text}
                </NavLink>
            </li>
        )
    }
}

export default FooterLink;