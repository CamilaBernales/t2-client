import React from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import AlertService from "../alertService/Alert";

const user = require("../api/users");
const {
  authenticateUser,
} = require("../../actions/userAction");

const Login = () => {
  const { register, handleSubmit, errors } = useForm();
  const dispatch = useDispatch();
  const history = useHistory()

  const authUser = async (data, e) => {
    e.preventDefault();
    try {
      const response = await user.authenticate(data);
      if (response.status === 200) {
        e.target.reset();
        AlertService({
          success: true,
          successMessage: "Usuario autenticado!",
        }).then(() => {
          dispatch(authenticateUser(response.data));
          localStorage.setItem('user', JSON.stringify(response.data))
          history.push('/backoffice/profile')
        });
      } else {
        AlertService({
          error: true,
          errorMessage: "Por favor revise sus datos",
        });
      }
    } catch (error) {
      console.log(error);
      AlertService({
        error: true,
        errorMessage:
          "No es posible conectarse al servidor, intente nuevamente en unos minutos.",
      });
    }
  };

  return (
    <div className="container mt-5">
      <div className="form-login">
        <h2>Ingrese con su cuenta</h2>
        <form
          onSubmit={handleSubmit(authUser)}
          action="s"
          method="POST"
          id="loginForm"
          className="clearfix"
        >
          <div className="field">
            <input
              type="email"
              name="email"
              placeholder="E-mail"
              ref={register({
                required: {
                  value: true,
                  message: "Por favor, complete los campos.",
                },
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: "Dirección de email no válida.",
                  maxLength: {
                    value: 30,
                    message: "Su e-mail debe tener máximo 30 caracteres",
                  },
                },
              })}
            />
            <span className="text-danger text-small">
              {errors?.email?.message}
            </span>
          </div>
          <div className="field">
            <input
              type="password"
              name="password"
              placeholder="Contraseña"
              ref={register({
                required: {
                  value: true,
                  message: "Por favor, complete los campos.",
                },
              })}
            />
            <span className="text-danger text-small">
              {errors?.password?.message}
            </span>
          </div>
          <div className="inline clearfix">
            <button type="submit" value="Send Messager" className="btn-primary">
              Ingresar
            </button>
            <p>
              <Link to="/account/recoverpassword">
                ¿Olvidaste la contraseña?
              </Link>
            </p>
          </div>
          <hr />
          <p>
            ¿Aún no tiene una cuenta?{" "}
            <Link to="/register">¡Registrate ahora!</Link>
          </p>
        </form>
      </div>
    </div>
  );
};

export default Login;
