const baseService = require("./baseService");
const {
  BASE_URL,
  NEW_DATA_ENDPOINT,
  NEW_CREATE_ENDPOINT,
  NEW_EDIT_ENDPOINT,
} = require("./endpoints");
const {
  getNewRemove,
  newDeletedSuccess,
  fetchNews,
  fetchNew
} = require("../../actions/newsAction");

export function deleteNew(id) {
  return async (dispatch) => {
    dispatch(getNewRemove(id));
    try {
      await baseService.baseDeleteRequest(
        `${BASE_URL}${NEW_DATA_ENDPOINT}/${id}`
      );
      dispatch(newDeletedSuccess());
    } catch (error) {
      console.log(error);
    }
  };
}

const create = async (data) => {
  try {
    return await baseService.basePostRequest(
      `${BASE_URL}${NEW_CREATE_ENDPOINT}`,
      data
    );
  } catch (error) {
    if (error.response) {
      return error.response;
    } else if (error.request) {
      throw new Error(`Can't connect with server`);
    } else {
      console.log("Error", error.message);
      throw new Error(`Can't connect with server`);
    }
  }
};

const edit = async (data, id) => {
  try {
    return await baseService.basePatchRequest(
      `${BASE_URL}${NEW_EDIT_ENDPOINT}/${id}`,
      data
    );
  } catch (error) {
    if (error.response) {
      return error.response;
    } else if (error.request) {
      throw new Error(`Can't connect with server`);
    } else {
      console.log("Error", error.message);
      throw new Error(`Can't connect with server`);
    }
  }
};

export function getNews() {
  return async (dispatch) => {
    try {
      let res = await baseService.baseGetRequest(
        `${BASE_URL}${NEW_DATA_ENDPOINT}`
      );
      let news =[];
      res.data.data.forEach((element) =>{
        const newDate = new Date(element.createdAt)
        const dateToShow = `${newDate.getDate()}/${newDate.getMonth()}/${newDate.getFullYear()}`
        news.push({
          id: element.id,
          title: element.title,
          createdAt: dateToShow,
          category: element.category,
          content: element.content,
          image:element.image
        })
      })
      dispatch(fetchNews(news));
    } catch (error) {
      console.log(error);
    }
  };
}

export function getNew(id) {
  return async (dispatch) => {
    try {
      let res = await baseService.baseGetRequest(
        `${BASE_URL}${NEW_DATA_ENDPOINT}/${id}:`
      );
      dispatch(fetchNew(res.data));
    } catch (error) {
      console.log(error);
    }
  };
}
export { create, edit };
