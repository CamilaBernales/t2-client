const Axios = require("axios");

const baseGetRequest = (url) => {
  Axios.interceptors.request.use((config) => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers = {
        Authorization: "Bearer " + token,
      };
    }
    return config;
  });

  return Axios.get(url);
};

const basePostRequest = (url, data) => {
  Axios.interceptors.request.use((config) => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers = {
        Authorization: "Bearer " + token,
      };
    }
    return config;
  });

  Axios.interceptors.response.use((res) => {
    try {
      const token = res.headers.authorization.split(" ")[1];
      localStorage.setItem("token", token.toString());
      return res;
    } catch (error) {
      return res;
    }
  });

  return Axios.post(url, data);
};

const basePatchRequest = (url, data) => {
  Axios.interceptors.request.use((config) => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers = {
        Authorization: "Bearer " + token,
      };
    } else {
      throw new Error("token is missing.");
    }
    return config;
  });

  Axios.interceptors.response.use((res) => {
    try {
      const token = res.headers.authorization.split(" ")[1];
      localStorage.setItem("token", token.toString());
      return res;
    } catch (error) {
      return res;
    }
  });

  return Axios.patch(url, data);
};

const baseDeleteRequest = (url, data) => {
  Axios.interceptors.request.use((config) => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers = {
        Authorization: "Bearer " + token,
      };
    } else {
      throw new Error("token is missing.");
    }
    return config;
  });
  Axios.interceptors.response.use((res) => {
    try {
      const token = res.headers.authorization.split(" ")[1];
      localStorage.setItem("token", token.toString());
      return res;
    } catch (error) {
      return res;
    }
  });

  return Axios.delete(url, data);
};

module.exports = {
  baseGetRequest,
  basePostRequest,
  basePatchRequest,
  baseDeleteRequest,
};
