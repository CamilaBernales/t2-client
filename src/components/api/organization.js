const baseService = require('./baseService')
const { BASE_URL, ORGANIZATION_DATA_ENDPOINT } = require('./endpoints') 

const getData = async ()=>{
    try {
        return await baseService.baseGetRequest(`${BASE_URL}${ORGANIZATION_DATA_ENDPOINT}`)
    } catch (error) {
        if(error.response){
            return error.response
        }else if (error.request){
            throw new Error(`Can't connect with server`)
        } else {
            console.log('Error', error.message);
            throw new Error(`Unable to get organization data`)
        }
    }
};

const edit = async (data, id) => {
    try {
        return await baseService.basePatchRequest(`${BASE_URL}${ORGANIZATION_DATA_ENDPOINT}/${id}`, data)
    } catch (error) {
        if(error.response){
            return error.response
        }else if (error.request){
            throw new Error(`Can't connect with server`)
        } else {
            console.log('Error', error.message);
            throw new Error(`Unable to get organization data`)
        }
    }
}

module.exports = {
    getData,
    edit
};
