const baseService = require("./baseService");
const { BASE_URL, SLIDERS_DATA_ENDPOINT, SLIDERS_CREATE_ENDPOINT, SLIDERS_DELETE_ENDPOINT} = require("./endpoints");
const getSliders = async () => {
  try {
    return await baseService.baseGetRequest(
      `${BASE_URL}${SLIDERS_DATA_ENDPOINT}`
    );
  } catch (error) {
    if (error.response || error.request) {
      throw new Error(`Sorry, an error ocurred`);
    }
  }
};

const create = async (data) => {
  try {
    return await baseService.basePostRequest(
      `${BASE_URL}${SLIDERS_CREATE_ENDPOINT}`, data
    );
  } catch (error) {
    if (error.response) {
      return error.response;
    } else if (error.request) {
      throw new Error(`Can't connect with server`);
    } else {
      console.log("Error", error.message);
      throw new Error(`Can't connect with server`);
    }
  }
};

const deleteSlide = async (id) => {
    try {
     return await baseService.baseDeleteRequest(
        `${BASE_URL}${SLIDERS_DELETE_ENDPOINT}/${id}`
      );
    } catch (error) {
      if (error.response) {
        return error.response;
      } else if (error.request) {
        throw new Error(`Can't connect with server`);
      } else {
        console.log("Error", error.message);
        throw new Error(`Can't connect with server`);
      }
    }
  };

module.exports = {
  getSliders,
  create,
  deleteSlide
};
