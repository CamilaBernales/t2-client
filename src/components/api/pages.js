const baseService = require("./baseService");
const {
  BASE_URL,
  PAGES_DATA_ENDPOINT,
  PAGE_EDIT_ENDPOINT,
} = require("./endpoints");
const { fetchPage, getDataPage } = require("../../actions/pageAction");

export function getPage(page) {
  return async (dispatch) => {
    dispatch(fetchPage(page));
    try {
      let res = await baseService.baseGetRequest(
        `${BASE_URL}${PAGES_DATA_ENDPOINT}${page}`
      );
      dispatch(getDataPage(res.data));
    } catch (error) {
      console.log(error);
    }
  };
}

const getPages = async () => {
  try {
    return await baseService.baseGetRequest(
      `${BASE_URL}${PAGES_DATA_ENDPOINT}`
    );
  } catch (error) {
    if (error.response) {
      return error.response;
    } else if (error.request) {
      throw new Error(`Can't connect with server`);
    } else {
      console.log("Error", error.message);
      throw new Error(`Unable to get organization data`);
    }
  }
};

const edit = async (data) => {
  try {
    return await baseService.basePatchRequest(
      `${BASE_URL}${PAGE_EDIT_ENDPOINT}/${data.id}`,
      data
    );
  } catch (error) {
    if (error.response) {
      return error.response;
    } else if (error.request) {
      throw new Error(`Can't connect with server`);
    } else {
      console.log("Error", error.message);
      throw new Error(`Unable to get organization data`);
    }
  }
};
export { getPages, edit };
