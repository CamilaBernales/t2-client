const baseService = require('./baseService')
const { BASE_URL, REGISTER_USER_ENDPOINT, AUTHENTICATE_USER_ENDPOINT, USER_DATA_ENDPOINT, USER_EDIT_ENDPOINT } = require('./endpoints') 

const register = async (data)=>{
    try {
        return await baseService.basePostRequest(`${BASE_URL}${REGISTER_USER_ENDPOINT}`, data)
    } catch (error) {
        if(error.response){
            return error.response
        }else if (error.request){
            throw new Error(`Can't connect with server`)
        } else {
            console.log('Error', error.message);
            throw new Error(`Can't connect with server`)
        }
    }
}

const authenticate = async (data)=>{
    try {
        return await baseService.basePostRequest(`${BASE_URL}${AUTHENTICATE_USER_ENDPOINT}`, data)
    } catch (error) {
        if(error.response){
            return error.response
        }else if (error.request){
            throw new Error(`Can't connect with server`)
        } else {
            console.log('Error', error.message);
            throw new Error(`Can't connect with server`)
        }
    }
}

const getData = async ()=>{
    try {
        const token = localStorage.getItem('token')
        return await baseService.baseGetRequest(`${BASE_URL}${USER_DATA_ENDPOINT}/${token}`)
    } catch (error) {
        if(error.response){
            return error.response
        }else if (error.request){
            return {error: `Can't connect with server`}
        } else {
            console.log('Error', error.message);
        }
    }
}

const edit = async (data)=>{
    try {
        const token = localStorage.getItem('token')
        return await baseService.basePatchRequest(`${BASE_URL}${USER_EDIT_ENDPOINT}/${token}`, data)
    } catch (error) {
        if(error.response){
            return error.response
        }else if (error.request){
            return {error: `Can't connect with server`}
        } else {
            console.log('Error', error.message);
            return {error: error.message}
        }
    }
}

module.exports = {
    register,
    authenticate,
    getData,
    edit
};