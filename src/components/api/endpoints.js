// url
const BASE_URL = "http://localhost:3000";

// User related endpoints
const REGISTER_USER_ENDPOINT = "/auth/register";
const AUTHENTICATE_USER_ENDPOINT = "/auth/login";
const USER_DATA_ENDPOINT = "/users";
const USER_EDIT_ENDPOINT = "/users";

// Organization related endpoints
const ORGANIZATION_DATA_ENDPOINT = '/organization/public';

// News related endpoints
const NEW_DATA_ENDPOINT = '/news';
const NEW_CREATE_ENDPOINT = '/news';
const NEW_EDIT_ENDPOINT = '/news';

//public
const SLIDERS_DATA_ENDPOINT = "/slider";
const SLIDERS_CREATE_ENDPOINT = "/slider";
const SLIDERS_DELETE_ENDPOINT = "/slider";
// Campaings related endpoints

//pages
const PAGES_DATA_ENDPOINT = '/pages';
const PAGE_EDIT_ENDPOINT = '/pages';

module.exports = {
  BASE_URL,
  REGISTER_USER_ENDPOINT,
  AUTHENTICATE_USER_ENDPOINT,
  USER_DATA_ENDPOINT,
  ORGANIZATION_DATA_ENDPOINT,
  USER_EDIT_ENDPOINT,
  SLIDERS_DATA_ENDPOINT,
  SLIDERS_CREATE_ENDPOINT,
  SLIDERS_DELETE_ENDPOINT,
  NEW_DATA_ENDPOINT,
  NEW_CREATE_ENDPOINT,
  NEW_EDIT_ENDPOINT,
  PAGES_DATA_ENDPOINT,
  PAGE_EDIT_ENDPOINT
}
