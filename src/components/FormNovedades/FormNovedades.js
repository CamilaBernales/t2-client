import React, { Fragment, useEffect, useState } from "react";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-base64";
import { useForm } from "react-hook-form";
import AlertService from "../alertService/Alert";
import { useHistory } from 'react-router';
const news = require( "../api/new");

function setSrc() {
  document.getElementById("imageContent").onchange = function (e) {
    let reader = new FileReader();

    reader.readAsDataURL(e.target.files[0]);

    reader.onload = function () {
      let preview = document.getElementById("preview"),
        image = document.createElement("img");

      image.src = reader.result;

      preview.innerHTML = "";
      preview.append(image);
    };
  };
}

const FormNovedades = (props) => {
  const history = useHistory();
  const { register, handleSubmit, setValue, errors } = useForm();
  const [newToEdit] = useState(props.location.newToEdit);
  const [editImage, setEditImage] = useState(false);

  useEffect(() => {
    if (newToEdit) {
      setValue("title", newToEdit.title);
      setValue("category", newToEdit.category);
      setValue("content", newToEdit.content);
    }
  }, [newToEdit, setValue]);

  const onSubmit = async (data) => {
    
    if (!props.location.newToEdit) {
      try {
        const formData = new FormData();
        formData.append("title", data.title);
        formData.append("category", data.category);
        formData.append("content", data.content);
        formData.append("image", data.image[0]);

        const response = await news.create(formData);
        if (response.status === 200) {
          AlertService({
            success: true,
            successMessage: "Novedad creada!",
          }).then(() => {
            history.push('/backoffice/news');
            //window.location.pathname = "/backoffice/news";
          });
        } else {
          AlertService({
            error: true,
            errorMessage:
              "No es posible crear la novedad, intente nuevamente en unos minutos",
          });
        }
      } catch (error) {
        console.log(error);
        AlertService({
          error: true,
          errorMessage:
            "No es posible crear la novedad, intente nuevamente en unos minutos",
        });
      }
    } else {
      try {
        const formData = new FormData();
        formData.append("id", newToEdit.id);
        formData.append("title", data.title);
        formData.append("category", data.category);
        formData.append("content", data.content);
        if (editImage) {
          formData.append("image", data.image[0]);
        }

        const response = await news.edit(formData, newToEdit.id);
        if (response.status === 200) {
          AlertService({
            success: true,
            successMessage: "Novedad actualizada!",
          }).then(() => {
            history.push('/backoffice/news');
            //window.location.pathname = "/backoffice/news";
          });
        } else {
          AlertService({
            error: true,
            errorMessage:
              "No es posible actualizar la novedad, intente nuevamente en unos minutos",
          });
        }
      } catch (error) {
        console.log(error);
        AlertService({
          error: true,
          errorMessage:
            "No es posible actualizar la novedad, intente nuevamente en unos minutos",
        });
      }
    }
  };

  return (
    <Fragment>
      <div className="container">
        <div className="form-group">
          <h2 className="mt-4">Novedades</h2>
        </div>
        <form className="clearfix" onSubmit={handleSubmit(onSubmit)}>
          <div className="row">
            <div className="col">
              <div className="form-group row">
                <label htmlFor="title" className="col-sm-2 col-form-label">
                  T&iacute;tulo
                </label>
                <div className="col-sm-10">
                  <input
                    name="title"
                    type="text"
                    className="form-control"
                    ref={register({
                      required: { value: true, message: "Complete este campo" },
                    })}
                  />
                  <span className="text-danger text-small">
                    {errors?.title?.message}
                  </span>
                </div>
              </div>
              <div className="form-group row">
                <label className="col-sm-2 col-form-label">
                  Categor&iacute;a
                </label>
                <div className="col-sm-10">
                  <select
                    name="category"
                    className="custom-select"
                    ref={register({
                      required: {
                        value: true,
                        message: "Seleccione una categoria",
                      },
                    })}
                  >
                    <option></option>
                    <option value="Novedad">Novedad</option>
                    <option value="Evento">Evento</option>
                    <option value="Información">Información</option>
                  </select>
                  <span className="text-danger text-small">
                    {errors?.category?.message}
                  </span>
                </div>
              </div>
              <div className="form-group row">
                <label className="col-sm-2 col-form-label">Contenido</label>
                <div className="col-sm-10">
                  <CKEditor
                    id
                    editor={ClassicEditor}
                    data="<p></p>"
                    onInit={(editor) => {
                      editor.setData(newToEdit ? newToEdit.content : "");
                      // You can store the "editor" and use when it is needed.
                      console.log("Editor is ready to use!", editor);
                    }}
                    onChange={(event, editor) => {
                      const data = editor.getData();
                      setValue("content", data, {
                        shouldValidate: true,
                        shouldDirty: true,
                      });
                    }}
                  />
                  <div className="d-none">
                    <input
                      name="content"
                      type="text"
                      className="form-control"
                      ref={register({
                        required: {
                          value: true,
                          message: "Complete este campo",
                        },
                      })}
                    />
                  </div>
                  <span className="text-danger block text-small text-center">
                    {errors.content && errors?.content.message}
                  </span>
                </div>
              </div>
              <div className="form-group">
                {!newToEdit ? (
                  <div>
                    <input
                      name="image"
                      type="file"
                      className="form-control-file"
                      id="imageContent"
                      onClick={setSrc}
                      accept="image/x-png,image/gif,image/jpeg"
                      ref={register({
                        required: {
                          value: true,
                          message: "Seleccione una imagen para su novedad",
                        },
                      })}
                    />
                    <span className="text-danger block text-small text-center">
                      {errors.image && errors?.image.message}
                    </span>
                    <div id="preview"></div>
                  </div>
                ) : (
                  <div>
                    {!editImage && (
                      <div>
                        <div className="col-sm-3 col-form-label p-0 text-left mb-3">
                          <label>Imagen de portada</label>
                        </div>
                        <div className="row d-flex justify-content-center mb-3">
                          <img src={newToEdit.image} alt="pic of news"/>
                        </div>
                        <button
                          onClick={(e) => setEditImage(true)}
                          className="btn-primary"
                        >
                          Editar
                        </button>
                      </div>
                    )}
                    {editImage && (
                      <div>
                        <input
                          name="image"
                          type="file"
                          className="form-control-file"
                          id="imageContent"
                          onClick={setSrc}
                          accept="image/x-png,image/gif,image/jpeg"
                          ref={register({
                            required: {
                              value: true,
                              message: "Seleccione una imagen para su novedad",
                            },
                          })}
                        />
                        <span className="text-danger block text-small text-center">
                          {errors.image && errors?.image.message}
                        </span>
                        <div id="preview"></div>
                      </div>
                    )}
                  </div>
                )}
              </div>
            </div>
          </div>
          <div>
            <button type="submit" className="btn btn-primary mb-4">
              GUARDAR
            </button>
          </div>
        </form>
      </div>
    </Fragment>
  );
};

export default FormNovedades;
