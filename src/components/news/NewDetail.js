import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getNew } from "../api/new";
import "../pages/pages.css";
import "./news.css";
import moment from "moment";
const NewDetail = () => {
  let { id } = useParams();
  const dispatch = useDispatch();
  useEffect(() => {
    // api request
    const fetchNew = () => dispatch(getNew(id));
    fetchNew();
    // eslint-disable-next-line
  }, []);
  const newFetched = useSelector((state) => state.news.new);
  const { title, content, image, createdAt, category } = newFetched;
  const newContent = { __html: content };
  return (
    <div className="container m-auto">
      <h1 className="mt-5 text-center text-capitalize">{title}</h1>
      <h5 className="text-left my-2">
        Publicado: {moment(createdAt).format("DD/MM/YYYY")}
      </h5>
      <h5 className="text-left my-2">
        Categoria: {category}
      </h5>

      <div className="my-2 m-auto d-flex justify-content-center align-items-center">
        <img className="news-image" src={image} alt="post" />
      </div>
      <div className="news-content" dangerouslySetInnerHTML={newContent}></div>
    </div>
  );
};

export default NewDetail;
