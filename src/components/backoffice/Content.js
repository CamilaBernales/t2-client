import React from "react";
import { Redirect, Switch, useRouteMatch } from "react-router-dom";
import IndexProfile from "../Profile/indexProfile";
import EditUser from "../editUser/EditUser";
import newsList from "../NewsList/TableNews";
import createNew from "../FormNovedades/FormNovedades";
import editOng from "../FormEditOrganization/Form";
import IndexOrganization from "../Organization/IndexOrganization";
import TablePages from "../pages/Table";
import EditPage from "../pages/edit";
import SliderList from "../FormSlider/IndexTableSliders";
import SliderNew from "../FormSlider/FormSlider";
import PrivateRouteAdmin from "../privateRoute/PrivateRouteAdmin"
import PrivateRoute from "../privateRoute/PrivateRoute"
export default () => {
  const { path } = useRouteMatch();
  return (
    <div className="col-lg-9">
      <div className="account-content dashboard">
        <Switch>
          <PrivateRoute exact path={`${path}`}>
            <Redirect to="/backoffice/profile" />
          </PrivateRoute>

          <PrivateRouteAdmin exact path={`${path}/slider`} component={SliderList} />
          {/* {!user && <Redirect to="/login" />} */}
          <PrivateRouteAdmin exact path={`${path}/slider/create`} component={SliderNew} />
          {/* {!user && <Redirect to="/login" />} */}
          <PrivateRoute exact path={`${path}/profile/edit`} component={EditUser}>
            {/* {!user && <Redirect to='/login'/>} */}
          </PrivateRoute>
          <PrivateRoute exact path={`${path}/profile`} component={IndexProfile}>
            {/* {!user && <Redirect to='/login'/>} */}
          </PrivateRoute>
          <PrivateRouteAdmin exact path={`${path}/news/create`} component={createNew}>
            {/* {!user && <Redirect to="/login" />} */}
          </PrivateRouteAdmin>
          <PrivateRouteAdmin exact path={`${path}/news`} component={newsList}>
            {/* {!user && <Redirect to="/login" />} */}
          </PrivateRouteAdmin>
          <PrivateRouteAdmin exact path={`${path}/ong/edit`} component={editOng}>
            {/* {!user && <Redirect to="/login" />} */}
          </PrivateRouteAdmin>
          <PrivateRouteAdmin exact path={`${path}/ong`} component={IndexOrganization}>
            {/* {!user && <Redirect to="/login" />} */}
          </PrivateRouteAdmin>
          <PrivateRouteAdmin exact path={`${path}/pages`} component={TablePages}>
            {/* {!user && <Redirect to="login" />} */}
          </PrivateRouteAdmin>
          <PrivateRouteAdmin exact path={`${path}/pages/edit`} component={EditPage}>
            {/* {!user && <Redirect to="/login" />} */}
          </PrivateRouteAdmin>
        </Switch>
      </div>
    </div>
  );
};
