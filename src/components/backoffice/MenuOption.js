import React from "react";
import { NavLink, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

const MenuOption = (props) => {
  const isAdmin = useSelector((state) => state.user.user);
  const location = useLocation();
  const style = props.option.private ? { display: "none" } : {};
  return (
    <li
      style={isAdmin.roleId === 1 ? null : style}
      className={location.pathname === props.option.route ? "active" : ""}
    >
      <NavLink to={`${props.option.route}`} activeClassName="active">
        {props.option.text}
      </NavLink>
    </li>
  );
};

export default MenuOption;
