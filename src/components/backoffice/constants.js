const baseOptions = [
  {
    text: "Mi Perfil",
    route: "/backoffice/profile",
    private: false,
  },
];

const adminOptions = [
  {
    text: "Novedades",
    route: "/backoffice/news",
    private: true,
  },
  {
    text: "Organizacion",
    route: "/backoffice/ong",
    private: true,
  },
  {
    text: "Páginas",
    route: "/backoffice/pages",
    private: true,
  },
  {
    text: "Slider",
    route: "/backoffice/slider",
    private: true,
  },
];

export { baseOptions, adminOptions };
