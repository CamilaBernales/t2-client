import React from 'react';
import Sidebar from './Sidebar';
import Content from './Content.js';
import './backoffice.css'

class Backoffice extends React.Component {

  render() {
    return (
      <div className='backoffice mt-5'>
        <div className="account-wrapper">
          <div className="container">
            <div className="row">
              <Sidebar />
              <Content />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Backoffice;
