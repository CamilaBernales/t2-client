import React from 'react';
import MenuManger from './menuManager';
import MenuOption from './MenuOption';

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      role: "admin",
      menu: [],
    };
  }

  componentDidMount() {
    const menu = new MenuManger(this.state.role).getMenu();
    this.setState({
      menu: menu,
    });
  }

  render() {
    const { menu } = this.state;
    return (
      <div className="col-lg-3">
        <nav className="account-bar">
          <ul>
            {menu &&
              menu.map((element, index) => {
                return (
                  <MenuOption
                    option={element}
                    location={this.props.match}
                    key={index}
                  />
                );
              })}
          </ul>
        </nav>
      </div>
    );
  }
}

export default Sidebar;