import { baseOptions, adminOptions } from "./constants";

class MenuManager {
  baseOptions = baseOptions;
  adminOptions = adminOptions;

  constructor(role) {
    this.role = role;
  }

  getMenu() {
    if (this.role === "admin") {
      return this.baseOptions.concat(this.adminOptions);
    }
    return this.baseOptions;
  }
}

export default MenuManager;
