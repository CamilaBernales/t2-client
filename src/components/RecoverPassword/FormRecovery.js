import React, {Fragment} from 'react'
import {useForm} from 'react-hook-form'
import SuggestRegister from './SuggestRegister'


const FormRecovery = ( )=> {

    const {register, handleSubmit, errors} = useForm()

    const onSubmit = (e)=>{
        // TO API
        e.preventDefault()
        e.target.reset()
    }
    return(
        <Fragment>
            <div className='form-login form-register'>
                <h2 className='text-left'>Recuperar Contraseña</h2>
                <form className='clearfix' onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-group">
                        <label className='text-left'>INGRESE SU EMAIL</label>
                        <input 
                            type="email" 
                            name="email"  
                            className='form-control'
                            ref={register({
                                required: {value:true, message: 'Ingrese una dirección de email'},
                                pattern: {
                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                    message: 'Dirección de email inválida'
                                }
                            })}/>
                        <span className='text-danger text-small'>
                            {errors?.email?.message}
                        </span>
                    </div>
                    <input className="btn btn-primary" type="submit" value="Enviar"/>
                </form><hr/>
                <SuggestRegister/>            
            </div>
        </Fragment>
    )
}

export default FormRecovery