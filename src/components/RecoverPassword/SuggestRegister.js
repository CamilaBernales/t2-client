import React, {Fragment} from 'react'
import { Link } from 'react-router-dom'

const SuggestRegister = () => {
    return(
        <Fragment>
            <div>
                <label >No tiene cuenta?!</label>
                <Link to='/register'>
                    Registrate aquí
                </Link>
            </div>
        </Fragment>
    )
}
export default SuggestRegister