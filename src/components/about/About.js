import React, { useEffect, useState } from 'react';
import organization from '../api/organization';
import Project from './Project';
import Contact from '../ContactForm/Contact'

function About(){
    const [ aboutText, setAboutText ] = useState()

    const projects = [
        {
            title: 'Cuarentena solidaria',
            description: 'Ayudanos a ayudar.'
        },
        {
            title: 'Enseñame algo que no pueda googlear',
            description: 'Destinado al personal docente.'
        },          
        {
            title: 'Programa de líderes',
            description: 'Capacitacion y coaching.'
        },
    ]

    useEffect(() => {
        const getData = async () => {
          try {
            const response = await organization.getData()
            setAboutText(response.data.description)
          } catch (error) {
            setAboutText('backup text')
            }
        }
    
        getData()
      }, []);

      return(
        <div>
        <section className="top-site">
			<div className="container">
                <h1>{aboutText}</h1>
			</div>
		</section>
        <section className="statics section">
            <h2 className="title">{projects.length} ejes principales de acción</h2>
					<div className="description">
                        <p>Actualmente contamos con las siguientes campañas de ayuda.</p>
                    </div>
					<div className="statics-content">
						<div className="row d-flex justify-content-center">
                            {projects && projects.map((element, index)=>{
                                return <Project project={element} key={index}/>
                            })}
					</div>
				</div>
			</section><hr/>
            <section>
                 <Contact/>           
            </section>
        </div>
    )
}

export default About;