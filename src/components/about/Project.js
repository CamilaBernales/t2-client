import React from 'react';

const Projects = (props) => {
    const { project } = props
    
    return(
        <div className="col-lg-3 col-sm-6 col-6">
            <div className="statics-item">
                <h3>{project.title}</h3>
                <div className="statics-item-desc">
                    <p>{project.description}</p>
                </div>
            </div>
        </div>
    )
};

export default Projects;