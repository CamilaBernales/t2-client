import React from 'react';
import { useSelector } from 'react-redux';
import {Link} from 'react-router-dom';
import Avatar from './avatar.png';

const IndexProfile = () =>{

    const user = useSelector(state => state.user)
    return(
        <div className="account-content profile">
            <h3 className='account-title'>Mi Perfil</h3>
            <div className="account-main profile">
                   <img  src={Avatar} alt=""/>
                        <div className="author-title">
                            <h1>{user.user.lastName +', ' + user.user.firstName}</h1>
                        </div><br/>
                        <div className="author-info">
                            <h4>{user.user.email}</h4>
                        </div>
            </div>
            <Link to='/backoffice/profile/edit' className="btn-primary mb-4">Editar Usuario</Link>
        </div>
    )
}

export default IndexProfile