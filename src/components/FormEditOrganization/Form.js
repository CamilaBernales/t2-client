import React, {Fragment, useEffect, useState} from 'react';
import {useForm} from 'react-hook-form';
import BtnSend from './BtnSend';
import Organization from '../api/organization';
import AlertService from '../alertService/Alert';

const Form = () => {

    const {register, handleSubmit, errors} = useForm()
    const [organization, setOrganization] = useState({})
    const [id, setId] = useState()
    const [editLogo, setEditLogo] = useState(false)

    useEffect( ()=> {
        const getData = async () => {
            try {
                const response = await Organization.getData()
                const data = response.data
                
                if(response.status ===200){
                    setId(data.id)  
                    setOrganization(data)
                }else{
                    throw new Error()
                }
            } catch (error) {
                console.log(error);
            }
        }
           getData()
           // eslint-disable-next-line
    }, [])

    const onSubmit = async (data, e) =>{
        try {
            const formData = new FormData();
            formData.append("id", id);
            formData.append("name", data.name);
            formData.append("address", data.address);
            formData.append("phone", data.phone);
            formData.append('description', data.description);
            formData.append('welcomeText', data.welcomeText);
            formData.append("facebookUrl", data.facebookUrl);
            formData.append("instagramUrl", data.instagramUrl);
            formData.append("linkedinUrl", data.linkedinUrl);
            if (editLogo) {
                formData.append("image", data.image[0]);
            }
            const response = await Organization.edit(formData, id);
            if (response.status === 200) {
                AlertService({
                    success: true,
                    successMessage: "Organización actualizada!",
                }).then(() => {
                    window.location.pathname = "/backoffice/ong";
                });
            } else {
                AlertService({
                    error: true,
                    errorMessage:
                    "No es posible actualizar la organización, intente nuevamente en unos minutos",
                });
            }
      } catch (error) {
        console.log(error);
        AlertService({
            error: true,
            errorMessage:
            "No es posible actualizar la organización, intente nuevamente en unos minutos",
        });
      }
    }

    return(
        <Fragment>
            <div className='form-login form-register'>
                    <h2 className='mt-4'>Editar la Organizacion</h2>
                    <form id='registerForm' className='clearfix' onSubmit={handleSubmit(onSubmit)}>
                        <div className="form-group row">
                            <label className='col-sm-2 col-form-label'>Nombre</label>
                            <div className="col-sm-10">
                                <input 
                                    type='text' 
                                    name='name'  
                                    value={organization.name}
                                    onChange={ e => setOrganization(e.target.value)}
                                    className='form-contol-plaintext'
                                    ref={register({
                                        required: {value:true, message: 'Complete este campo'},
                                        maxLength: {value:30, message: 'El nombre debe tener maximo 30 caracteres'},
                                        minLength: {value:2, message: 'El nombre debe tener minimo 2 caracteres'}    
                                    })}/>
                                <span className='text-danger text-small'>
                                    {errors?.name?.message}
                                </span>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className='col-sm-2 col-form-label'>Dirección</label>
                            <div className="col-sm-10">
                                <input 
                                    type='text' 
                                    name='address' 
                                    onChange={ e => setOrganization(e.target.value)} 
                                    value={organization.address}
                                    className='form-contol-plaintext'
                                    ref={register({
                                        required: {value:true, message: 'Complete este campo'},
                                        maxLength: {value:30, message: 'La dirección alcanzó el máximo de caracteres'},
                                        minLength: {value:2, message: 'La direccion debe tener un minimo de 2 caracteres'}    
                                    })}/>
                            <span className='text-danger text-small'>
                                {errors?.address?.message}
                            </span>
                            </div>
                        </div>

                        <div className="form-group row">
                        <label className='col-sm-2 col-form-label'>Teléfono</label>
                            <div className="col-sm-10">
                                <input 
                                    type='number' 
                                    name='phone' 
                                    onChange={ e => setOrganization(e.target.value)}
                                    value={organization.phone}
                                    className='form-contol-plaintext'
                                    ref={register({
                                        required: {value:true, message: 'Complete este campo'},  
                                    })}/>
                            <span className='text-danger text-small'>
                                {errors?.phone?.message}
                            </span>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className='col-sm-2 col-form-label'>Descripcion</label>
                            <div className="col-sm-10">
                                <input 
                                    type='text' 
                                    name='description' 
                                    onChange={ e => setOrganization(e.target.value)} 
                                    value={organization.description}
                                    className='form-contol-plaintext'
                                    ref={register({
                                        required: {value:true, message: 'Complete este campo'},
                                        maxLength: {value:80, message: 'Alcanzó el máximo de caracteres'},
                                        minLength: {value:2, message: 'Debe ingresar un mínimo de 2 caracteres'}    
                                    })}/>
                            <span className='text-danger text-small'>
                                {errors?.description?.message}
                            </span>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className='col-sm-2 col-form-label'>Bienvenida</label>
                            <div className="col-sm-10">
                                <input 
                                    type='text' 
                                    name='welcomeText' 
                                    onChange={ e => setOrganization(e.target.value)} 
                                    value={organization.welcomeText}
                                    className='form-contol-plaintext'
                                    ref={register({
                                        required: {value:true, message: 'Complete este campo'},
                                        minLength: {value:2, message: 'Debe ingresar un mínimo de 2 caracteres'}    
                                    })}/>
                            <span className='text-danger text-small'>
                                {errors?.welcomeText?.message}
                            </span>
                            </div>
                        </div>
                        <div className="form-group row">
                        <label className='col-sm-2 col-form-label'>Facebook</label>
                            <div className="col-sm-10">
                                <input 
                                    type='text' 
                                    name='facebookUrl' 
                                    onChange={ e => setOrganization(e.target.value)}
                                    value={organization.facebookUrl}
                                    className='form-contol-plaintext'
                                    ref={register({
                                        required: {value:true, message: 'Complete este campo'},  
                                        maxLength: {value:255, message: 'La dirección alcanzó el máximo de caracteres'},
                                        minLength: {value:10, message: 'La direccion debe tener un minimo de 10 caracteres'}
                                    })}/>
                            <span className='text-danger text-small'>
                                {errors?.facebookUrl?.message}
                            </span>
                            </div>
                        </div>

                        <div className="form-group row">
                        <label className='col-sm-2 col-form-label'>Instagram</label>
                            <div className="col-sm-10">
                                <input 
                                    type='text' 
                                    name='instagramUrl' 
                                    onChange={ e => setOrganization(e.target.value)}
                                    value={organization.instagramUrl}
                                    className='form-contol-plaintext'
                                    ref={register({
                                        required: {value:true, message: 'Complete este campo'},  
                                        maxLength: {value:255, message: 'La dirección alcanzó el máximo de caracteres'},
                                        minLength: {value:10, message: 'La direccion debe tener un minimo de 10 caracteres'}
                                    })}/>
                            <span className='text-danger text-small'>
                                {errors?.instagramUrl?.message}
                            </span>
                            </div>
                        </div>

                        <div className="form-group row">
                        <label className='col-sm-2 col-form-label'>LinkedIn</label>
                            <div className="col-sm-10">
                                <input 
                                    type='text' 
                                    name='linkedinUrl' 
                                    onChange={ e => setOrganization(e.target.value)}
                                    value={organization.linkedinUrl}
                                    className='form-contol-plaintext'
                                    ref={register({
                                        required: {value:true, message: 'Complete este campo'},  
                                        maxLength: {value:255, message: 'La dirección alcanzó el máximo de caracteres'},
                                        minLength: {value:10, message: 'La direccion debe tener un minimo de 10 caracteres'}
                                    })}/>
                            <span className='text-danger text-small'>
                                {errors?.linkedinUrl?.message}
                            </span>
                            </div>
                        </div>

                        <div>
                            {!editLogo &&
                                <div>
                                    <div className="col-sm-3 col-form-label p-0 text-left mb-3">
                                        <label>Logo</label>
                                    </div>
                                    <div className="row d-flex justify-content-center mb-3">
                                        <img src={organization.image} alt="organization logo" />
                                    </div>
                                    <div className='row d-flex justify-content-center mb-3'> 
                                        <button onClick={(e) => setEditLogo(true)} className="btn-primary">
                                            Editar
                                        </button>
                                    </div>
                                </div>
                            }
                            {editLogo &&
                                <div className="form-group row">
                                <label className='col-sm-2 col-form-label'>Logo</label>
                                    <div className="col-sm-10">
                                        <input 
                                            type="file" 
                                            name="image" 
                                            id=""
                                            ref={register({
                                                required: {value:true, message: 'El logo de la organizacion es requerido'}
                                            })}/>
                                    <span className='text-danger text-small'>
                                        {errors?.image?.message}
                                    </span>
                                    </div>
                                </div>
                            }
                        </div>

                        <BtnSend/>
                    </form>    
            </div>
        </Fragment>
    )
}
export default Form