import React, { useEffect, useState } from 'react'
import {Link} from 'react-router-dom'
import Organization from '../api/organization'
import './organization.css'

const IndexOrganization = () =>{

    const [organization, setOrganization] = useState({})
    useEffect ( () => {
        const getData = async () => {
            try {
                const response = await Organization.getData()
                const data = response.data
                if(response.status ===200){  
                    setOrganization(data)
                }else{
                    throw new Error()
                }
            } catch (error) {
                console.log(error);
            }
        }
        
           getData()
           
       },[])
    return(
        <div className="account-content profile">
            <h3 className='account-title'>Datos de la ONG</h3>
            <div className="account-main organization">
                <img className='m-auto' src={organization.image} alt=""/>
                <div className="author-title">
                    <h2 className='text-uppercase'>{organization.name }</h2>
                </div><br/>
                <div className="author-info">
                    <ul>
                        <li><i className="fa fa-map-marker mr-2" aria-hidden="true"></i>{organization.address}</li>
                        <li><i className="fa fa-phone mr-2" aria-hidden="true"></i>(+54) {organization.phone}</li>
                    </ul>
                </div>              
            </div>

            <div className="follow">
                    <ul>
                        <li className='facebook'>
                            <a href={organization.facebookUrl}>
                                <i className='fab fa-facebook' aria-hidden='true'/>
                            </a>
                        </li>
                        <li className='instagram'>
                            <a href={organization.instagramUrl}>
                                <i className='fab fa-instagram' aria-hidden='true'/>
                            </a>
                        </li>
                        <li className='linkedin'>
                            <a href={organization.linkedinUrl}>
                                <i className='fab fa-linkedin' aria-hidden='true'/>
                            </a>
                        </li>               
                    </ul>
            </div>

            <Link to='/backoffice/ong/edit' className="btn-primary mb-4 mt-4">Editar Organizacion</Link>
        </div>
    )
}

export default IndexOrganization