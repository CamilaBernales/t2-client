import swal from '@sweetalert/with-react'

const AlertService = (props)=>{
    
    if(props.success){
        return swal({
            text: props.successMessage,
            icon: 'success'
        })
    }

    if(props.error){
        return swal({
            text: props.errorMessage,
            icon: "error",
        })
    }

}

export default AlertService;
