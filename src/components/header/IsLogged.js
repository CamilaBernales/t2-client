import React, { useEffect, useState } from "react";
import { logged } from "./headerRoutes";
import { Link, NavLink } from "react-router-dom";
import Organization from '../api/organization';
import { useDispatch  } from "react-redux";
import { logoutUser } from '../../actions/userAction';
import './header.css'

const style = {
  headerShadow: {
    boxShadow: '1px 1px 10px lightGrey'
  }
}

const IsLogged = (props) => {

  const [ ong, setOngData ] = useState()
  const [ isOpen, setOpen ] = useState(false)
  const [ isSubMenuOpen, setSubMenuOpen ] = useState()
  const dispatch = useDispatch()

  useEffect(() => {
    const getData = async () => {
      try {
        const response = await Organization.getData()
        if(props.mounted){
          if(response.status === 200){
            setOngData(response.data)
          }else {
            throw new Error()
          }
        }
      } catch (error) {
        if(props.mounted){
          setOngData({
            image: 'images/assets/logo.png'
          })
        }
      }
    }

    const handleResize = ()=>{
      const width = window.innerWidth
      if(width > 992){
        setSubMenuOpen(true)
      }else {
        setSubMenuOpen(false)
      }
    }

    getData()
    handleResize()
    window.addEventListener('resize', handleResize)
  }, [props.mounted]);

  const openMenu = (e)=>{
    setOpen(!isOpen)
  }

  const logout = ()=>{
    try {
      const token = localStorage.getItem('token')
      if(token){
        dispatch(logoutUser())
        localStorage.removeItem('token')
        localStorage.removeItem('user')

      }
    } catch (error) {
        return
      }
  }

  const openSubMenu = (e)=>{
    e.preventDefault()
    if(window.innerWidth > 992){
      return
    }else {
      setSubMenuOpen(!isSubMenuOpen)
    }
  }

  return (
    <div className={isOpen ? 'menu-open' : ''} >
      <div id='wrapper'>
        <header id="header" className="site-header" style={style.headerShadow}>
        {ong && 
              <div className="top-header clearfix">
                  <div className="container d-flex justify-content-end">
                    <p className='mr-3'>Seguinos!</p>
                    <ul className="socials-top">
                      <li>
                        <a href={ong.facebookUrl}>
                          <i className="fab fa-facebook" aria-hidden="true"/>
                        </a>
                      </li>
                      <li>
                        <a href={ong.instagramUrl}>
                          <i className="fab fa-instagram" aria-hidden="true"/>
                        </a>
                      </li>
                      <li>
                        <a href={ong.linkedinUrl}>
                          <i className="fab fa-linkedin" aria-hidden="true"/>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              }
          <div className="content-header">
            <div className="container">
              <div className="site-brand">
                <Link to="/" className='d-flex justify-content-between'>
                  {ong && <img src={ong.image} alt="Logo"  className='logoZg'/> }
                </Link>
              </div>
              <div className="right-header">
                <nav className={isOpen ? 'main-menu open m-0' : 'main-menu m-0'}>
                  <button className={isOpen ? 'c-hamburger c-hamburger--htx is-active' : 'c-hamburger c-hamburger--htx' } onClick={()=> openMenu()} >
                    <span></span>
                  </button>
                  <ul>
                  {logged.map((option, index) => {
                      if(option.text === 'Actividades'){
                        return (
                          <li key={index}>
								            <NavLink to="/" onClick={(e)=> openSubMenu(e)} on className='text-left dropdown-toggle'>{option.text}</NavLink>
                            <ul id={isSubMenuOpen ? 'custom-sub-menu-open' : 'custom-sub-menu-hide'}>
                              {option.activities.map((element, index)=>{
                                return (
                                  <li key={index}><NavLink activeClassName="selected" to={element.route}>{element.text}</NavLink></li>
                                )
                              })}
                            </ul>
                          </li>
                        )
                      }else {
                        return (
                          <li key={index}>
                          <NavLink
                            activeClassName="selected"
                            className={`${option.className} text-left`}
                            to={option.route}
                          >
                            {option.text}
                          </NavLink>
                        </li>
                        )
                      }
                    })}
                  </ul>
                </nav>
                <div className="login login-button">
                  <Link to='/' className='btn-primary' onClick={ () => logout()}>Cerrar sesión</Link>
                </div>
              </div>
            </div>
          </div>
        </header>
      </div>
    </div>
  );
};

export default IsLogged;
