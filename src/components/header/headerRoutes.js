const logged = [
  {
    text: "Inicio",
    route: "/home",
    className: "m-auto",
  },
  {
    text: "Nosotros",
    route: "/us",
    className: "m-auto",
  },
  {
    text: "Actividades",
    route: "/",
    activities: [
      {
        text: "Cuarentena Solidaria",
        route: "/page/cuarentena-solidaria",
      },
      {
        text: "Programa de líderes",
        route: "/page/programa-de-lideres",
      },
      {
        text: "Teach me something I can’t Google",
        route: "/page/teach-me-something",
      },
    ],
    className: "m-auto",
  },
  {
    text: "Novedades",
    route: "/news",
    className: "m-auto",
  },
  {
    text: "Escritorio",
    route: "/backoffice",
    className: "m-auto",
  },
];

const notLogged = [
  {
    text: "Inicio",
    route: "/home",
    className: "m-auto",
  },
  {
    text: "Nosotros",
    route: "/us",
    className: "m-auto",
  },
  {
    text: "Actividades",
    route: "/",
    activities: [
      {
        text: "Cuarentena Solidaria",
        route: "/page/cuarentena-solidaria",
      },
      {
        text: "Programa de líderes",
        route: "/page/programa-de-lideres",
      },
      {
        text: "Cuarentena Solidaria",
        route: "/page/teach-me-something",
      },
    ],
    className: "m-auto",
  },
  {
    text: "Novedades",
    route: "/news",
    className: "m-auto",
  },
];

export { logged, notLogged };
