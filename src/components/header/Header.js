import React, { useEffect, useState } from "react";
import NotLogged from "./NotLogged";
import IsLogged from "./IsLogged";
import { useSelector } from "react-redux";
const Header = () => {
  const [isLogin, setIsLogin] = useState(false);
  const [notLoggedMounted, setNotLoggedMounted] = useState(false);
  const [isLoggedMounted, setIsLoggedMounted] = useState(false);
  const { user } = useSelector((state) => state.user);
  useEffect(() => {
    if (Object.entries(user).length > 0) {
      setIsLogin(true);
      setNotLoggedMounted(false);
      setIsLoggedMounted(true);
    } else {
      setIsLogin(false);
      setNotLoggedMounted(true);
      setIsLoggedMounted(false);
    }
  }, [user]);

  return (
    <div>
      {!isLogin ? (
        <NotLogged mounted={notLoggedMounted} />
      ) : (
        <IsLogged mounted={isLoggedMounted} />
      )}
    </div>
  );
};

export default Header;
