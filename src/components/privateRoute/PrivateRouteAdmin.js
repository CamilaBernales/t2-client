import React, { useState, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
const PrivateRouteAdmin = ({ component: Component, ...props }) => {
  const [validPermit, setValidPermit] = useState(false);
  const [loading, setLoading] = useState(true);
  const user = JSON.parse(localStorage.getItem("user"));
  useEffect(() => {
    const uservalidation = () => {
      if (user && Object.entries(user).length > 0 && user.roleId === 1) {
        setValidPermit(true);
        setLoading(false);
      } else {
        setValidPermit(false);
        setLoading(false);
      }
    };
    uservalidation();
    // eslint-disable-next-line
  }, []);
  return loading ? null : (
    <Route
      {...props}
      render={(routeProps) => {
        return !user ? (
          <Redirect to="/login" />
        ) : !validPermit ? (
          <Redirect to="/backoffice/profile" />
        ) : (
          <Component {...routeProps} {...props} />
        );
      }}
    />
  );
};
export default PrivateRouteAdmin;
