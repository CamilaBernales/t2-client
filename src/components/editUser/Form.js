import React, { Fragment, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import AlertService from "../alertService/Alert";
import Users from "../api/users";
import { editUser } from "../../actions/userAction";

const Form = () => {
  const { register, handleSubmit, errors } = useForm();
  const history = useHistory();
  const { user } = useSelector((state) => state.user);
  const [newUser, setNewUser] = useState(user);
  const dispatch = useDispatch();

  const onSubmit = async (data, e) => {
    e.preventDefault();
    const finalData = {
      ...user,
      ...data,
    };
    try {
      const response = await Users.edit(finalData);
      if (response.status === 200) {
        AlertService({ success: true, successMessage: "Usuario actualizado!" });
        dispatch(editUser(finalData));
        history.push("/backoffice/profile");
      } else {
        AlertService({
          error: true,
          errorMessage:
            "No es posible actualizar el usuario, intente nuevamente en unos minutos.",
        });
      }
    } catch (error) {
      console.log(error);
      AlertService({
        error: true,
        errorMessagge:
          "No es posible actualizar el usuario, intente nuevamente en unos minutos.",
      });
    }
    e.target.reset();
  };

  return (
    <Fragment>
      <div className="campaign-form form-update">
        <form id="editUserForm" onSubmit={handleSubmit(onSubmit)}>
          <h4 className="mt-3">Editar usuario</h4>
          <div className="form-group">
            <label className="float-left">Nombre</label>
            <input
              type="text"
              name="firstName"
              className="form-control form-control-lg"
              value={newUser.firstName}
              onChange={(e) => {
                setNewUser({
                  roleId: user.roleId,
                  [e.target.name]: e.target.value,
                });
              }}
              ref={register({
                required: { value: true, message: "Complete este campo" },
                maxLength: {
                  value: 30,
                  message: "El nombre debe tener maximo 30 caracteres",
                },
                minLength: {
                  value: 2,
                  message: "El nombre debe tener minimo 2 caracteres",
                },
              })}
            />
            <span className="text-danger text-small">
              {errors?.firstName?.message}
            </span>
          </div>
          <br />

          <div className="form-group">
            <label className="float-left">Apellido</label>
            <input
              type="text"
              name="lastName"
              className="form-control form-control-lg"
              value={newUser.lastName}
              onChange={(e) => {
                setNewUser({
                  roleId: user.roleId,
                  [e.target.name]: e.target.value,
                });
              }}
              ref={register({
                required: { value: true, message: "Complete este campo" },
                maxLength: {
                  value: 30,
                  message: "El apellido debe tener maximo 30 caracteres",
                },
                minLength: {
                  value: 2,
                  message: "El apellido debe tener minimo 2 caracteres",
                },
              })}
            />
            <span className="text-danger text-small">
              {errors?.lastName?.message}
            </span>
          </div>
          <br />
          <button className="btn-primary mt-4" type="submit">
            Actualizar usuario
          </button>
        </form>
      </div>
    </Fragment>
  );
};

export default Form;
