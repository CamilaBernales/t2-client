import React, { Fragment } from 'react';
import Form from './Form';

const EditUser = () => {
    return(
        <Fragment>
            <main id="main" className="site-main">
                <Form/>
            </main>
        </Fragment>
    )
}   

export default EditUser;