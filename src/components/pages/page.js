import React,{useEffect} from "react";
import { useSelector, useDispatch } from "react-redux";
import { getPage } from "../api/pages";
import './pages.css'
const Page = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    const page = window.location.pathname;
    const fetchPage = () => dispatch(getPage(page));
    fetchPage();
  }, [window.location.pathname]);
   const pageFetched = useSelector((state) => state.pages.pageData);
   const {title, content} = pageFetched
   const contentToRender = {__html: content}

  return(
    <div className="container m-auto">
      <div className='page-content mt-5' dangerouslySetInnerHTML={contentToRender}></div>
    </div>
  );
};

export default Page;
