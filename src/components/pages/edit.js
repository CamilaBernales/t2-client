import React, { Fragment, useEffect } from "react";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-base64";
import { useForm } from "react-hook-form";
import AlertService from "../alertService/Alert";
import { edit } from "../api/pages";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";

const EditPage = (props) => {
  const { register, handleSubmit, setValue, errors } = useForm();
  const pageToEdit = useSelector((state) => state.pages.pageToEdit);
  const history = useHistory();

  useEffect(() => {
    if (pageToEdit) {
      setValue("title", pageToEdit.title);
      setValue("content", pageToEdit.content);
    }
  }, [pageToEdit, setValue]);

  const onSubmit = async (data) => {
    const finalData = {
      id: pageToEdit.id,
      ...data,
    };
    try {
      const response = await edit(finalData);
      if (response.status === 200) {
        AlertService({
          success: true,
          successMessage: "Página actualizada!",
        }).then(() => {
          history.push("/backoffice/pages");
        });
      } else {
        AlertService({
          error: true,
          errorMessage:
            "No es posible actualizar la página, intente nuevamente en unos minutos",
        });
      }
    } catch (error) {
      console.log(error);
      AlertService({
        error: true,
        errorMessage:
          "No es posible actualizar la página, intente nuevamente en unos minutos",
      });
    }
  };

  return (
    <Fragment>
      <div className="container">
        <div className="form-group">
          <h2 className="mt-4">
            Página: {`${pageToEdit ? pageToEdit.title : ""}`}{" "}
          </h2>
        </div>
        <form className="clearfix" onSubmit={handleSubmit(onSubmit)}>
          <div className="row">
            <div className="col">
              <div className="form-group row">
                <label className="col-sm-2 col-form-label">Contenido</label>
                <div className="col-sm-10">
                  <CKEditor
                    id
                    editor={ClassicEditor}
                    data="<p></p>"
                    onInit={(editor) => {
                      editor.setData(
                        pageToEdit.content ? pageToEdit.content : ""
                      );
                    }}
                    onChange={(event, editor) => {
                      const data = editor.getData();
                      setValue("content", data, {
                        shouldValidate: true,
                        shouldDirty: true,
                      });
                    }}
                  />
                  <div className="d-none">
                    <input
                      name="content"
                      type="text"
                      className="form-control"
                      ref={register({
                        required: {
                          value: true,
                          message: "Complete este campo",
                        },
                      })}
                    />
                  </div>
                  <span className="text-danger block text-small text-center">
                    {errors.content && errors?.content.message}
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div>
            <button type="submit" className="btn btn-primary mb-4">
              GUARDAR
            </button>
          </div>
        </form>
      </div>
    </Fragment>
  );
};

export default EditPage;
