import React, { Fragment, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getPages } from "../api/pages";
import AlertService from "../alertService/Alert";
import { getPageToEdit } from "../../actions/pageAction";
import { useHistory } from 'react-router';

const Table = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [pages, setPages] = useState([]);

  useEffect(() => {
    async function getData() {
      try {
        const response = await getPages();
        if (response.status === 200) {
          setPages(response.data);
        } else {
          throw new Error("Unable to get pages data.");
        }
      } catch (error) {
        AlertService({
          error: true,
          errorMessage:
            "No es posible obtener la información, intente nuevamente en unos minutos",
        }).then(() => {
          history.push('/backoffice');
          //window.location.pathname = "/backoffice";
        });
      }
    }
    getData();
  }, []);
 
  return (
    <Fragment>
      <div className="account-content backed-campaigns account-table">
        <h3 className="account-title">Páginas</h3>
        <div className="account-main">
          <table className="table">
            <thead className="thead-dark">
              <tr>
                <th>TITULO</th>
                <th className="text-center">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              {pages &&
                pages.map((element, index) => {
                  return (
                    <tr key={index}>
                      <td>{element.title}</td>
                      <td className="d-flex justify-content-center">
                        <NavLink
                          to={{
                            pathname: "/backoffice/pages/edit",
                          }}
                          onClick={() => dispatch(getPageToEdit(element))}
                          className="btn btn-info"
                        >
                          Editar
                        </NavLink>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
    </Fragment>
  );
};

export default Table;
