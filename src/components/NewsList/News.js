import React,{useEffect} from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getNews } from "../api/new";
import './main.css'
const News = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    const fetchData = () => dispatch(getNews());
    fetchData();
    // eslint-disable-next-line
  }, []);
  const news = useSelector((state) => state.news.news);

  return (
    <div className="container m-auto">
      <h1 className=" mt-5 py-3 text-center">Novedades</h1>
      <div className="row m-auto ">
        {news.map((post) => (
          <div key={post.id} className="col col-12 col-sm-12 col-md-6 col-lg-4 text-center">
            <div className="card my-3 py-4">
              <img
                className="card-img-top box-height"
                src={post.image}
                alt="Formato Imagen Incorrecto"
              />
              <div className="card-body">
                <h3 className="card-title">{post.title}</h3>
                <Link to={`/news/detail/${post.id}`} className="btn btn-primary mt-3">
                  Ver más
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default News;
