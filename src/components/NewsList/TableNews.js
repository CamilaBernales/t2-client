import React, { Fragment, useEffect } from "react";
import { Link } from "react-router-dom";
import BtnDelete from "./BtnDelete";
import BtnEdit from "./BtnEdit";
import { useSelector, useDispatch } from "react-redux";
import { getNews } from "../api/new";
import './main.css'

const TableNews = () => {
  const dispatch = useDispatch();
  const news = useSelector((state) => state.news.news);
  useEffect(() => {
    const fetchData = () => dispatch(getNews());
    fetchData();
    // eslint-disable-next-line
  }, []);
   
  const dateSlice = (date) => {
    return date.slice(0,10)
  }

  return (
    <Fragment>
      <div className="account-content backed-campaigns account-table">
        <h3 className="account-title">Novedades</h3>
        <div className="account-main table-container">
          <table className="table">
            <thead className="thead-dark">
              <tr>
                <th>FECHA</th>
                <th className="text-center">TITULO</th>
                <th className="text-center">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              {news.map((n) => {
                return (
                  <tr key={n.id}>
                    <td>{dateSlice(n.createdAt)}</td>
                    <td className="text-center">{n.title}</td>
                    <td className="d-flex justify-content-center">
                      <BtnEdit new={n}/>
                      <BtnDelete id={n.id} />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
      <Link className="btn-primary mb-4 ml-3 float-xl-left" to="news/create">
        Nueva
      </Link>
    </Fragment>
  );
};

export default TableNews;
