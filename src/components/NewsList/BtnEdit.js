import React, { Fragment } from 'react'
import { NavLink } from 'react-router-dom'

const BtnEdit = (props) => {
    return (
        <Fragment>
            <NavLink to={{pathname: '/backoffice/news/create', newToEdit: props.new}} className='btn btn-info' type="button" >
                EDITAR
            </NavLink>
        </Fragment>
    )
}

export default BtnEdit