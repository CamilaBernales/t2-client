const news = [
  {
    title: "Lorem Ipsum",
    content:
      " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    category: "news",
    subtitle:
      "is simply dummy text of the printing and typesetting industry. is simply dummy text of the printing and typesetting industry.",
    image: "http://lorempixel.com/400/200/sports/1",
  },
  {
    title: "Lorem Ipsum",
    content:
      " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    category: "news",
    subtitle:
      "is simply dummy text of the printing and typesetting industry is simply dummy text of the printing and typesetting industry.",
    image: "http://lorempixel.com/400/200/sports/2",
  },
  {
    title: "Lorem Ipsum",
    content:
      " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    category: "news",
    subtitle:
      "is simply dummy text of the printing and typesetting industry is simply dummy text of the printing and typesetting industry.",
    image: "http://lorempixel.com/400/200/sports/3",
  },
  {
    title: "Lorem Ipsum",
    content:
      " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    category: "news",
    subtitle:
      "is simply dummy text of the printing and typesetting industry is simply dummy text of the printing and typesetting industry.",
    image: "http://lorempixel.com/400/200/sports/4",
  },
  {
    title: "Lorem Ipsum",
    content:
      " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    category: "news",
    subtitle:
      "is simply dummy text of the printing and typesetting industry is simply dummy text of the printing and typesetting industry.",
    image: "http://lorempixel.com/400/200/sports/5",
  },
  {
    title: "Lorem Ipsum",
    content:
      " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    category: "news",
    subtitle:
      "is simply dummy text of the printing and typesetting industry is simply dummy text of the printing and typesetting industry.",
    image: "http://lorempixel.com/400/200/sports/6",
  },
  {
    title: "Lorem Ipsum",
    content:
      " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    category: "news",
    subtitle:
      "is simply dummy text of the printing and typesetting industry is simply dummy text of the printing and typesetting industry.",
    image: "http://lorempixel.com/400/200/sports/3",
  },
];

export { news };
