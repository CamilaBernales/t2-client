import React, { Fragment } from "react";
import { deleteNew } from "../api/new";
import swal from "sweetalert";
import { useDispatch } from "react-redux";
const BtnDelete = ({ id }) => {
  const dispatch = useDispatch();
  const confirmeAction = () => {
    swal({
      title: "¿Estas seguro de que quieres eliminar este post?",
      text: "No podrás revertir esta acción.",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        swal("Post elimnado con éxito", {
          icon: "success",
        });
        dispatch(deleteNew(id));
      }
    });
  };
  return (
    <Fragment>
      <input
        className="btn btn-danger ml-2"
        type="button"
        onClick={confirmeAction}
        value="ELIMINAR"
      />
    </Fragment>
  );
};

export default BtnDelete;
