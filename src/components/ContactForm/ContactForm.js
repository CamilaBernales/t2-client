import React, { Fragment } from 'react'
import {useForm} from 'react-hook-form'

const FormContact = () => {

	const {register, handleSubmit, errors} = useForm()

	const onSubmit = async (data, e) => {
		e.preventDefault()
		console.log(data);
	}
    return(
        <Fragment>
            <div className="col-lg-8">
				<div className="form-contact">
					<h2>Dejanos tu mensaje</h2>
						<form id="contactForm" className="clearfix" onSubmit={handleSubmit(onSubmit)}>
							<div className="clearfix">
								<div className="field align-left">
									<input 
										  type="text" 
										  name="name" 
										  placeholder='Nombre'
										  ref={register({
											required: {value:true, message: 'Complete este campo'},
											maxLength: {value:15, message: 'Alcanzaste el máximo de caracteres'},
											minLength: {value:2, message: 'El nombre debe tener mínimo 2 caracteres'}  
										  })}/>
									<span className='text-danger text-small'>
										{errors?.name?.message}
									</span>
				                </div>
								<div className="field align-right">
									<input 
										  type="text" 
										  name="email" 
										  placeholder="Email"
										  ref={register({
												required: {value:true, message: 'Ingrese una direccion de email'},
												pattern: {
												value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
												message: "Direccion de email inválida"
                                                }
                                        })}/>
									<span className='text-danger text-small'>
                          				{errors?.email?.message}
                       				 </span>
								</div>
							</div>
								<div className="field">
									<input
										type='text' 
										name='subject'
										placeholder='Asunto' 
										ref={register({
											required: {value:true, message: 'Complete este campo'},
											maxLength: {value:30, message: 'Alcanzaste el máximo de caracteres'},
											minLength: {value:2, message: 'Ingresa al menos dos caracteres'}    
										})}/>
									<span className='text-danger text-small'>
										{errors?.subject?.message}
									</span>
								</div>
							<div className="field-textarea">
								<textarea 
										rows="8"
									    placeholder="Mensaje" 
										name='message'
										ref={register({
											required: {value:true, message: 'El mensaje no puede quedar vacio!!'},
											maxLength: {value:50, message: 'Alcanzaste el máximo de caracteres'},
											minLength: {value:5, message: 'Ingresa al menos un mínimo de 5 caracteres'}
										})}
										/>
								    <span className='text-danger text-small'>
										{errors?.message?.message}
									</span>
							</div>
							<button type="submit" value="Send Messager" className="btn-primary">Enviar Mensaje</button>
						</form>
				</div>
			</div>
        </Fragment>
    )
}
export default FormContact