import React, { Fragment, useEffect, useState } from 'react'
import Organization from '../api/organization'

const ContactInfo = () => {

    const [organization, setOrganization]= useState({})

    useEffect( () => {
        getData()
    }, [])
    
    const getData = async () =>{
        try {
            const response  = await Organization.getData()
            if (response.status) {
                setOrganization(response.data)
            } else {
                throw new Error()
            }
        } catch (error) {
            console.log(error)
        }
    }


    return(
        <Fragment>
            <div className="col-lg-4">
                <div className="contact-info">
                    <h3>Informacion de Contacto</h3>
                        <ul>
                            <li><i className="fa fa-map-marker" aria-hidden="true"></i>{organization.address}</li>
                            <li><i className="fa fa-phone" aria-hidden="true"></i>(+54) {organization.phone}</li>
                            <li><i className="fa fa-mobile" aria-hidden="true"></i>(+54) {organization.phone} </li>
                            <li><i className="fa fa-envelope" aria-hidden="true"></i>{organization.facebookUrl}</li>
                        </ul>
                    <div className="contact-desc"><p>{organization.description}</p></div>
                </div>
            </div>
        </Fragment>
    )
}

export default ContactInfo