import React, { Fragment } from 'react'
import ContactForm from './ContactForm'
import ContactInfo from './ContactInfo'

const Contact = () => {
    return(
        <Fragment>
            <div className="page-content contact-content pt-4 pb-5">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 main-content">
                            <div className="entry-content">
                                <div className="row">
                                    <ContactForm/>
                                    <ContactInfo/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Contact