import React from "react";
import { Redirect, Route, Switch } from "react-router";
import Backoffice from "./backoffice/Backoffice";
import Form from "./Form/Form";
import RecoveryPassword from "./RecoverPassword/RecoveryPassword";
import Login from "./login/Login";
import About from "./about/About";
import Home from "./screenhome/Home";
import News from "./NewsList/News";
import Page from "./pages/page";
import NewDetail from "./news/NewDetail"

export default () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Home}>
          <Redirect to="/home" />
        </Route>
        <Route exact path="/home" component={Home} />
        <Route exact path="/news" component={News} />
        <Route exact path="/account/recoverpassword" component={RecoveryPassword}>
        </Route>
        <Route exact path="/us" component={About} />
        <Route exact path="/news/detail/:id" component={NewDetail} />
        <Route exact path="/page/:text" component={Page} />
        <Route exact path="/register" component={Form}>
        </Route>
        <Route exact path="/login" component={Login}>
        </Route>
        <Route path="/backoffice" component={Backoffice}>
        </Route>
      </Switch>
    </div>
  );
};
