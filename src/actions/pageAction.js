import {
  GET_PAGE,
  GET_PAGE_DATA,
  GET_PAGE_TO_EDIT,
} from "../types";

const fetchPage = (page) => ({
  type: GET_PAGE,
  payload: page,
});
const getDataPage = (data) => ({
  type: GET_PAGE_DATA,
  payload: data,
});
const getPageToEdit = (id) => ({
  type: GET_PAGE_TO_EDIT,
  payload: id,
});
export {fetchPage, getDataPage, getPageToEdit };
