import {
    AUTHENTICATE_USER,
    LOGOUT_USER,
    EDIT_USER,
} from "../types";
  
const authenticateUser = (user) => ({
    type: AUTHENTICATE_USER,
    payload: user,
});

const editUser = (newUser) => ({
    type: EDIT_USER,
    payload: newUser
})

const logoutUser = () => ({
    type: LOGOUT_USER
})


export { authenticateUser, editUser, logoutUser };
  