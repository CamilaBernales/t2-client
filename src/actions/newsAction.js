import {
  GET_NEWS,
  GET_NEW_REMOVE,
  NEW_DELETED_SUCCESS,
  NEW_DELETED_ERROR,
  GET_NEW,
} from "../types";

const fetchNews = (news) => ({
  type: GET_NEWS,
  payload: news,
});
const getNewRemove = (id) => ({
  type: GET_NEW_REMOVE,
  payload: id,
});
const newDeletedSuccess = () => ({
  type: NEW_DELETED_SUCCESS,
});
const fetchNew = (newDetail) => ({
  type: GET_NEW,
  payload: newDetail,
});
export { getNewRemove, newDeletedSuccess, fetchNews, fetchNew };
